# -*- coding: utf-8 -*-
import scrapy
import psycopg2 as sql
import os
import configparser
from datetime import datetime
from ..items import CargoItem, filter_strip


class LardiCargosSpider(scrapy.Spider):
    name = 'lardi_cargos'
    allowed_domains = ['lardi-trans.com']

    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 16,
        'CONCURRENT_REQUESTS_PER_IP': 4,
        'CONCURRENT_REQUESTS': 32,
        'RETRY_HTTP_CODES': [500, 503, 504, 400, 403, 404, 408],
        'RETRY_TIMES': 10,
        'ROBOTSTXT_OBEY': False,
        'COOKIES_ENABLED': False,
        'DOWNLOAD_TIMEOUT': 3,
        'DOWNLOADER_MIDDLEWARES': {
            'freight_sites.middlewares.RandomProxy': 555
        },
        'ITEM_PIPELINES': {
            'freight_sites.pipelines.FreightSitesPipeline': 300
        }
    }

    def __init__(self, urls='', **kwargs):
        self.start_urls = urls.split(',')
        self._config = configparser.ConfigParser()
        self._config.read('../{}'.format(os.getenv('CONFIG_PATH')))
        self._conn = sql.connect(self._config.get('Default','db_path').replace('"', ''))
        self._cursor = self._conn.cursor()


    def parse(self, response):
        new_time = response.headers.get('date').decode()
        timestamp = datetime.strptime(new_time, "%a, %d %b %Y %X %Z").strftime("%s") + '000'
        url = response.request.url.split('=')
        url = url[0] + '='

        self._cursor.execute("INSERT INTO urls_cargo (url, last_item) VALUES (%(url)s, %(last_time)s) ON CONFLICT (url) DO UPDATE SET last_item = %(last_time)s", {'last_time': timestamp, 'url': url})
        
        list_of_cargos = response.xpath('//body/div')

        for cargo in list_of_cargos:
            l= scrapy.loader.ItemLoader(item=CargoItem(), selector=cargo)
            l.default_input_processor = scrapy.loader.processors.MapCompose(filter_strip)

            geo = cargo.xpath('.//span[@class="ps_data_direction"]/text()').get().split(' - ')
            dateload = cargo.xpath('.//div[@class="ps_data_load_date__mobile-info"]/span/text()').get().split('\xa0\n–\xa0')
            dateload_to = dateload[1] if len(dateload) > 1 else 'false'
            is_new = 'true' if len(cargo.xpath('.//span[@class="ps_data_proposal__new"]')) > 0 else 'false'
            payment_info = cargo.xpath('.//div[@class="ps_data_payment__mobile"]/span[@class="ps_data_payment_info"]/text()')
            payment_info = payment_info.get() if len(payment_info) > 0 else 'false'
            payment_details = cargo.xpath('.//div[@class="ps_data_payment__mobile"]/span[@class="ps_data_payment_details"]/text()')
            payment_details = payment_details.get() if len(payment_details) > 0 else 'false'
            link = response.urljoin(cargo.xpath('.//a[@class="ps_options_list__item"]/@href').get())

            l.add_value('from_country', geo[0])
            l.add_value('to_country', geo[1])
            l.add_value('dateload_from', dateload[0])
            l.add_value('dateload_to', dateload_to)
            l.add_value('is_new', is_new)
            l.add_xpath('truck_type', './/div[@class="ps_data_transport__mobile"]/span/text()')
            l.add_value('payment_info', payment_info)
            l.add_value('payment_details', payment_details)
            l.add_xpath('cargo_data', './/div[@class="ps_data_cargo__mobile"]/div/span[not(span)]/text() | .//div[@class="ps_data_cargo__mobile"]/div/span//span/text()[1]')
            l.add_xpath('from_region', './/div[contains(@class, "ps_search-result_data-rout")]/div[@class="ps_data_rout__from"]/div/ul/li/span/text()')
            l.add_xpath('to_region', './/div[contains(@class, "ps_search-result_data-rout")]/div[@class="ps_data_rout__to"]/div/ul/li/span/text()')
            l.add_value('link', link)
            l.add_value('distance', '0')
            l.add_value('per_km', '0')
            l.add_value('site_type', 'Lardi Trans')
            l.add_value('url', url)
            l.add_value('timestamp', timestamp)

            yield l.load_item()


    def closed(self, reason):
        self._conn.commit()
        self._conn.close()
        self.logger.info('Spider Lardi closed!')