import asyncio
import os


async def test_process():
    os.chdir('freight_sites')
    url = 'https://lardi-trans.com/gruz/live/c640q1y1.html?showTimeFrom=1593880785000'
    scrapy_process = await asyncio.create_subprocess_exec('scrapy', 'crawl', 'lardi_cargos', '-o', 'cargos.json', '-a', 'urls={}'.format(url), stdout=asyncio.subprocess.PIPE)
    await scrapy_process.communicate()


asyncio.run(test_process())