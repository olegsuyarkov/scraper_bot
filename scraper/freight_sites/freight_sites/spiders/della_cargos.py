import scrapy
import psycopg2 as sql
import os
import configparser
from datetime import datetime
from ..items import CargoItem, filter_strip


class DellaCargosSpider(scrapy.Spider):
    name = 'della_cargos'
    allowed_domains = ['della.ua']

    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 16,
        'CONCURRENT_REQUESTS_PER_IP': 4,
        'CONCURRENT_REQUESTS': 32,
        'RETRY_HTTP_CODES': [500, 503, 504, 400, 403, 404, 408],
        'RETRY_TIMES': 10,
        'ROBOTSTXT_OBEY': False,
        'COOKIES_ENABLED': False,
        'DOWNLOAD_TIMEOUT': 3,
        'DOWNLOADER_MIDDLEWARES': {
            'freight_sites.middlewares.RandomProxy': 555
        },
        'ITEM_PIPELINES': {
            'freight_sites.pipelines.FreightSitesPipeline': 300
        }
    }

    def __init__(self, urls='', **kwargs):
        self.start_urls = urls.split(',')
        self._config = configparser.ConfigParser()
        self._config.read('../{}'.format(os.getenv('CONFIG_PATH')))
        self._conn = sql.connect(self._config.get('Default','db_path').replace('"', ''))
        self._cursor = self._conn.cursor()


    def parse(self, response):
        pass

    
    def closed(self, reason):
        self._conn.commit()
        self._conn.close()
        self.logger.info('Spider Della closed!')