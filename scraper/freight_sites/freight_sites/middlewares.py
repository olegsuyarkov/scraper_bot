# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

import re
import random
import base64
import logging
from scrapy import signals


class FreightSitesSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class FreightSitesDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


log = logging.getLogger('custom.proxy.middleware')


class RandomProxy(object):

    def __init__(self, settings):
        log.info('---INIT MIDDDLEWARE RandomProxy---')
        self.proxypool = {}
        self.current_proxy = None
        self.retry_count = 0

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)

    def process_request(self, request, spider):

        retry_stats = spider.crawler.stats
        retry_stats = retry_stats.get_value('retry/count')
        if retry_stats is not None:
            difference = retry_stats - self.retry_count
            self.retry_count = retry_stats
        else:
            difference = 0
        
        # get from spider
        conn = spider._conn
        cursor = spider._cursor
        config = spider._config

        if self.current_proxy is None:
            log.info('Proxy none, get new proxy from db')
            cursor.execute('SELECT proxy, errors FROM proxypool WHERE errors < %(error_limit)s AND use_flag IS FALSE', {'error_limit': int(config.get('Spiders', 'max_errors_per_proxy'))})
            for row in cursor:
                self.proxypool[row[0]] = row[1]
            choice_proxy = random.choice(list(self.proxypool.keys()))
            self.current_proxy = [choice_proxy, self.proxypool[choice_proxy]]
            cursor.execute('UPDATE proxypool SET use_flag = TRUE WHERE proxy = %(proxy)s', {'proxy': self.current_proxy[0]})
            conn.commit()
            log.info('Current proxy is {}'.format(self.current_proxy[0]))
            request.meta['proxy'] = self.current_proxy[0]
        else:
            if difference >= 1:
                log.info('Retry increment proxy errors')
                errors = int(self.current_proxy[1])
                errors += difference
                self.current_proxy[1] = errors

            log.info('Proxy is ready. Current proxy is {}'.format(self.current_proxy[0]))
            if int(self.current_proxy[1]) < int(config.get('Spiders', 'max_errors_per_proxy')):
                log.info('Proxy not have many errors yet')
                request.meta['proxy'] = self.current_proxy[0]
            else:
                log.info('Proxy is bad. Get new proxy')
                log.info('Current bad proxy is {}'.format(self.current_proxy[0]))
                cursor.execute('UPDATE proxypool SET errors = %(errors)s WHERE proxy = %(proxy)s', {'errors': self.current_proxy[1], 'proxy': self.current_proxy[0]})
                cursor.execute('UPDATE proxypool SET use_flag = false WHERE proxy = %(proxy)s', {'proxy': self.current_proxy[0]})
                conn.commit()
                self.proxypool.clear()
                cursor.execute('SELECT proxy, errors FROM proxypool WHERE errors < %(error_limit)s AND use_flag IS FALSE', {'error_limit': int(config.get('Spiders', 'max_errors_per_proxy'))})
                for row in cursor:
                    self.proxypool[row[0]] = row[1]
                choice_proxy = random.choice(list(self.proxypool.keys()))
                loop_flag = True
                while loop_flag:
                    log.info('Loop choice proxy')
                    if self.current_proxy[0] != choice_proxy:
                        log.info('Choice new proxy')
                        self.current_proxy[0] = choice_proxy
                        self.current_proxy[1] = self.proxypool[choice_proxy]
                        loop_flag = False
                        cursor.execute('UPDATE proxypool SET use_flag = TRUE WHERE proxy = %(proxy)s', {'proxy': self.current_proxy[0]})
                        conn.commit()
                        request.meta['proxy'] = self.current_proxy[0]
                    else:
                        log.info('Rerandom choice proxy')
                        choice_proxy = random.choice(list(self.proxypool.keys()))

    def process_response(self, request, response, spider):

        # get from spider
        conn = spider._conn
        cursor = spider._cursor

        cursor.execute('UPDATE proxypool SET use_flag = false WHERE proxy = %(proxy)s', {'proxy': self.current_proxy[0]})
        conn.commit()
        return response


    def process_exception(self, request, exception, spider):
        if 'proxy' not in request.meta:
            return
        else:
            log.info('Process exeption. Increment proxy errors')
            errors = int(self.current_proxy[1])
            errors += 1
            log.info('Errors of current proxy is {}'.format(str(errors)))
            self.current_proxy[1] = errors
            request.meta["exception"] = True