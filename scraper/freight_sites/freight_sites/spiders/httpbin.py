import scrapy
import psycopg2 as sql
import os
import configparser
import json

class HttpbinSpider(scrapy.Spider):
    name = 'httpbin'
    allowed_domains = ['httpbin.org']
    start_urls = ['https://httpbin.org/anything/{}'.format(i) for i in range(1, 10)]

    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 64,
        'CONCURRENT_REQUESTS_PER_IP': 16,
        'CONCURRENT_REQUESTS': 32,
        'RETRY_TIMES': 10,
        'ROBOTSTXT_OBEY': False,
        'COOKIES_ENABLED': False,
        'DOWNLOAD_TIMEOUT': 3,
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
            'freight_sites.middlewares.RandomProxy': 100,
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110
        }
    }

    def __init__(self, **kwargs):
        self._config = configparser.ConfigParser()
        self._config.read('../{}'.format(os.getenv('CONFIG_PATH')))
        self._conn = sql.connect(self._config.get('Default','db_path').replace('"', ''))
        self._cursor = self._conn.cursor()


    def parse(self, response):
        resp = json.loads(response.body)

        yield {
            'ip': resp['origin'],
            'url': resp['url']
        }


    def closed(self, reason):
        self._conn.commit()
        self._conn.close()
        self.logger.info('Spider HTTPbin closed!')