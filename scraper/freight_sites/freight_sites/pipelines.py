# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from itemadapter import ItemAdapter


class FreightSitesPipeline:

    def process_item(self, item, spider):
        adapter = ItemAdapter(item).asdict()
        cursor = spider._cursor
        cursor.execute("INSERT INTO cargos (from_country, to_country, dateload_from, dateload_to, is_new, truck_type, payment_info, payment_details, cargo_data, from_region, to_region, link, distance, per_km, site_type, url, timestamp) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", list(adapter.values()))
        return item
