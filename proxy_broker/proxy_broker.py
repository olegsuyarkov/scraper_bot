import asyncio
import logging
import sys
import os
import configparser
import psycopg2 as sql
from proxybroker import Broker
from pprint import pprint


logging.basicConfig(level=sys.argv[2] if len(sys.argv) > 2 else 'WARNING', format='%(filename)s  --- %(levelname)5s: %(message)s', stream=sys.stderr)


async def save_to_db(proxies, config: configparser.ConfigParser):
    proxypool = []
    while True:
            proxy = await proxies.get()
            if proxy is None:
                break
            if 'HTTPS' in proxy.types:
                proto = 'https'
            elif 'HTTP' in proxy.types:
                proto = 'http'
            row = '%s://%s:%d' % (proto, proxy.host, proxy.port)
            logging.info('Find and check proxy ' + row)
            proxypool.append((row,))
    conn = sql.connect(config.get('Default', 'db_path').replace('"', ''))
    cursor = conn.cursor()
    cursor.executemany("INSERT INTO proxypool (proxy, errors) VALUES (%s, 0) ON CONFLICT DO NOTHING", proxypool)
    conn.commit()
    conn.close()
    logging.info('Save new proxies to db proxypool')
    

def main():
    config = configparser.ConfigParser()
    config.read(os.getenv('CONFIG_PATH'))
    proxies = asyncio.Queue()
    broker = Broker(queue=proxies, timeout=int(config.get('Proxybroker', 'timeout_request')))
    tasks = asyncio.gather(broker.find(types=config.get('Proxybroker', 'allowed_types_proxy').split(','), limit=int(sys.argv[1]) if len(sys.argv) > 1 else int(config.get('Proxybroker', 'default_limit_proxies'))), save_to_db(proxies, config))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(tasks)


if __name__ == '__main__':
    main()