import psycopg2 as sql
import os
import configparser


config = configparser.ConfigParser()
config.read(os.getenv('CONFIG_PATH'))
conn_string = config.get('Default','db_path').replace('"', '')
conn = sql.connect(conn_string)

cursor = conn.cursor()

cursor.execute("""CREATE TABLE urls_cargo(
    id SERIAL PRIMARY KEY,
    url VARCHAR(355) UNIQUE,
    last_item BIGINT
)""")

# cursor.execute("""CREATE TABLE cargos(
#     id SERIAL PRIMARY KEY,
#     from_country VARCHAR(5),
#     to_country VARCHAR(5),
#     dateload_from VARCHAR(25),
#     dateload_to VARCHAR(25),
#     is_new BOOLEAN,
#     truck_type VARCHAR(25),
#     payment_info VARCHAR(25),
#     payment_details VARCHAR(55),
#     cargo_data VARCHAR(355),
#     from_region VARCHAR(100),
#     to_region VARCHAR(100),
#     link VARCHAR(255),
#     distance SMALLINT,
#     per_km REAL,
#     site_type VARCHAR(20),
#     url VARCHAR(355),
#     timestamp BIGINT
# )
# """)

# cursor.execute("""CREATE TABLE urls_truck_lardi(
#     url TEXT UNIQUE,
#     last_time INTEGER
# )""")

# cursor.execute("""
#     CREATE OR REPLACE FUNCTION trigger_set_timestamp()
#     RETURNS TRIGGER AS $$
#     BEGIN
#         NEW.last_update = NOW();
#         RETURN NEW;
#     END;
#     $$ LANGUAGE plpgsql;
# """)

# cursor.execute("""
#     CREATE TABLE IF NOT EXISTS proxypool(
#         id SERIAL PRIMARY KEY,
#         proxy VARCHAR(55) UNIQUE,
#         errors SMALLSINT,
#         use_flag BOOLEAN DEFAULT False,
#         last_update TIMESTAMPTZ NOT NULL DEFAULT NOW()
#     )
# """)

# cursor.execute("""
#     CREATE TRIGGER set_timestamp
#     BEFORE UPDATE ON proxypool
#     FOR EACH ROW
#     EXECUTE PROCEDURE trigger_set_timestamp();
# """)

conn.commit()

conn.close()