# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field
from scrapy.loader.processors import TakeFirst, MapCompose, Identity


def filter_strip(value):
    text = value.replace('\n', '')
    text = text.strip()
    return text


def filter_brackets(value):
    text = value.replace('(', '')
    text = text.replace(')', '')
    return text


class CargoItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    from_country = Field(
        output_processor = TakeFirst()
    )
    to_country = Field(
        output_processor = TakeFirst()
    )
    dateload_from = Field(
        output_processor = TakeFirst()
    )
    dateload_to = Field(
        output_processor = TakeFirst()
    )
    is_new = Field(
        output_processor = TakeFirst()
    )
    truck_type = Field(
        output_processor = TakeFirst()
    )
    payment_info = Field(
        output_processor = TakeFirst()
    )
    payment_details = Field(
        output_processor = TakeFirst()
    )
    cargo_data = Field(
        output_processor = Identity()
    )
    from_region = Field(
        input_processor = MapCompose(filter_strip ,filter_brackets)
    )
    to_region = Field(
        input_processor = MapCompose(filter_strip, filter_brackets)
    )
    link = Field(
        output_processor = TakeFirst()
    )
    distance = Field(
        output_processor = TakeFirst()
    )
    per_km = Field(
        output_processor = TakeFirst()
    )
    site_type = Field(
        output_processor = TakeFirst()
    )
    url = Field(
        output_processor = TakeFirst()
    )
    timestamp = Field(
        output_processor = TakeFirst()
    )